# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Assignment 4 Deliverables:

1. Provide Bitbucket read-only access to a4 repo, *must* include README.md, using Markdown
syntax.
2. Blackboard Links: a4 Bitbucket repo

#### Assignment Screenshots:

![Failed Validation](img/failed.png)

![Passed Validation](img/pass.png)
