// save as "<TOMCAT_HOME>\webapps\a4\WEB-INF\classes\crud\business\Customer.java"
package crud.business;

import java.io.Serializable;

public class Customer implements Serializable {

    private String fname;
    private String lname;
    private String email;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String balance;
	private String totalSales;
	private String notes;

    public Customer() 
	{
        fname = "";
        lname = "";
       	street = "";
		city = "";
		state = "";
		zip = "";
		phone = "";
		email = "";
		balance = "";
		totalSales = "";
		notes = "";
    }

    public Customer(String parFirstName, String parLastName, String parStreet, String parCity, String parState, String parZip, String parPhone, String parEmail, String parBalance, String parTotalSales, String parNotes) 
	{
        this.fname = parFirstName;
        this.lname = parLastName;
		this.street= parStreet;
		this.city=parCity;
		this.state=parState;
		this.zip=parZip;
		this.phone=parPhone;
		this.email = parEmail;
		this.balance=parBalance;
		this.totalSales=parTotalSales;
		this.notes=parNotes;
    }

    public String getFirstName() {
        return fname;
    }

    public void setFirstName(String parFirstName) {
        this.fname = parFirstName;
    }

    public String getLastName() {
        return lname;
    }

    public void setLastName(String parLastName) {
        this.lname = parLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String parEmail) {
        this.email = parEmail;
    }
	public String getStreet() {
        return street;
    }

    public void setStreet(String parStreet) {
        this.street = parStreet;
    }
	public String getCity() {
        return city;
    }

    public void setCity(String parCity) {
        this.city = parCity;
    }
	public String getState() {
        return state;
    }

    public void setState(String parState) {
        this.state = parState;
    }
	public String getZip() {
        return zip;
    }

    public void setZip(String parZip) {
        this.zip = parZip;
    }
	public String getPhone() {
        return phone;
    }

    public void setPhone(String parPhone) {
        this.phone = parPhone;
    }
	public String getBalance() {
        return balance;
    }

    public void setBalance(String parBalance) {
        this.balance = parBalance;
    }
	public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String parTotalSales) {
        this.totalSales = parTotalSales;
    }
	public String getNotes() {
        return notes;
    }

    public void setNotes(String parNotes) {
        this.notes = parNotes;
    }
}
